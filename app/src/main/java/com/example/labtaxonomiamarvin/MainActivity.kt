package com.example.labtaxonomiamarvin

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import com.example.labtaxonomiamarvin.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    //Views

    private lateinit var etAno: EditText
    private lateinit var btnVerificar: Button
    private lateinit var tvResultado: TextView
    private lateinit var binding: ActivityMainBinding


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        initView()

        btnVerificar.setOnClickListener {
            if (etAno.text.toString().isEmpty()) {
                Toast.makeText(this, "Campo vacío", Toast.LENGTH_LONG).show()
            } else {
                val ano = etAno.text.toString().toInt()
                when (ano) {
                    in 1930..1948 ->
                        tvResultado.text = "Silent Generation ${getEmoji(128529)}"
                    in 1949..1968 ->
                        tvResultado.text = "Baby Boom ${getEmoji(129297)}"
                    in 1969..1980 ->
                        tvResultado.text = "Generación X ${getEmoji(128526)}"
                    in 1981..1993 ->
                        tvResultado.text = "Generación Y \n Millenials \n ${getEmoji(128534)}"
                    in 1994..2010 ->
                        tvResultado.text = "Generación Z ${getEmoji(128523)}"
                    else -> tvResultado.text = "Edad fuera de rango"
                }
            }
        }


    }

    private fun initView() {
        etAno = binding.etAno
        btnVerificar = binding.btnVerificar
        tvResultado = binding.tvResultado
    }

    fun getEmoji(unicode: Int): String {
        return String(Character.toChars(unicode))
    }
}